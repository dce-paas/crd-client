/*
Copyright 2022.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1

import (
	v1 "k8s.io/api/apps/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

var (
	APPNameLabel        = "app.customer.com/appName"
	APPDisplayNameLabel = "app.customer.com/appDisplayName"
	ModuleNameLabel     = "app.customer.com/moduleName"
	DeploymentType      = "app.customer.com/deploymentType"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// ApplicationSpec defines the desired state of Application
type ApplicationSpec struct {
	// INSERT ADDITIONAL SPEC FIELDS - desired state of cluster
	// Important: Run "make" to regenerate code after modifying this file
	//+kubebuilder:validation:Pattern="(.){2,60}$"
	DisplayName string   `json:"displayName,omitempty"`
	Description string   `json:"description"`
	UserID      int      `json:"userID"`
	Modules     []Module `json:"modules,omitempty"`
}

// ApplicationStatus defines the observed state of Application
type ApplicationStatus struct {
	TotalModuleNumber    int32  `json:"totalModuleNumber,omitempty"`
	RunningModuleNumber  int32  `json:"runningModuleNumber,omitempty"`
	StartingModuleNumber int32  `json:"startingModuleNumber,omitempty"`
	StoppedModuleNumber  int32  `json:"stoppedModuleNumber,omitempty"`
	RollingUpdateNumber  int32  `json:"rollingUpdateNumber,omitempty"`
	Status               string `json:"status,omitempty"` // 应用状态 {Running| Stopped}
}

// Application is the Schema for the applications API
// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
// +kubebuilder:object:root=true
// +kubebuilder:subresource:status
// +kubebuilder:resource:shortName=app
// +kubebuilder:printcolumn:name="DISPLAYNAME",JSONPath=".spec.displayName",type="string"
// +kubebuilder:printcolumn:name="STATUS",JSONPath=".status.status",type="string"
// +kubebuilder:printcolumn:name="TOTAL",JSONPath=".status.totalModuleNumber",type="string"
// +kubebuilder:printcolumn:name="RUNNING",JSONPath=".status.runningModuleNumber",type="string"
// +kubebuilder:printcolumn:name="ROLLINGUPDATE",JSONPath=".status.rollingUpdateNumber",type="string"
type Application struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   ApplicationSpec   `json:"spec,omitempty"`
	Status ApplicationStatus `json:"status,omitempty"`
}

// ApplicationList contains a list of Application
// +kubebuilder:object:root=true
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
type ApplicationList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []Application `json:"items"`
}

// Proxy 服务出口代理设置,指定协议和内外部端口,自动调谐ingress tcp/udp configmap
type Proxy struct {
	Protocol   string `json:"protocol"`
	Port       int32  `json:"port"`
	TargetPort int32  `json:"targetPort"`
}

// ServiceConfig 服务引用ConfigMap配置信息
type ServiceConfig struct {
	ConfigGroup string `json:"configGroup,omitempty"`
	ConfigItem  string `json:"configItem,omitempty"`
	MountPath   string `json:"mountPath,omitempty"`
}

// Module 应用中的服务信息
type Module struct {
	Name           string            `json:"name"`
	AccessMode     string            `json:"accessMode,omitempty"`
	Proxies        []Proxy           `json:"proxies,omitempty"`
	ServiceConfigs []ServiceConfig   `json:"serviceConfigs,omitempty"`
	AppPkgID       string            `json:"appPkgID,omitempty"`
	Template       v1.DeploymentSpec `json:"template"`
}
