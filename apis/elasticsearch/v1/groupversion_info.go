// Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
// or more contributor license agreements. Licensed under the Elastic License;
// you may not use this file except in compliance with the Elastic License.

// Package v1 contains API Schema definitions for the app v1 API group
//+kubebuilder:object:generate=true
//+groupName=elasticsearch.k8s.elastic.co
package v1

import (
	"k8s.io/apimachinery/pkg/runtime/schema"
)

var (
	// GroupVersion is group version used to register these objects
	GroupVersion = schema.GroupVersion{Group: "elasticsearch.k8s.elastic.co", Version: "v1"}
)
