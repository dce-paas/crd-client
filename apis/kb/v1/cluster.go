package v1

import (
	"fmt"
	"github.com/pkg/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"net"
	"net/url"
)

const (
	Installing    ClusterAction = "installing"
	UnInstalling  ClusterAction = "unInstalling"
	Installed     ClusterAction = "installed"
	InstallFailed ClusterAction = "installFailed"

	DefaultCriType            = CriTypeContainerd
	CriTypeDocker     CriType = "docker"
	CriTypeContainerd CriType = "containerd"

	DefaultWorkDir string = "/var/lib"
)

type ClusterAction string

// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
// +genclient:nonNamespaced
type Cluster struct {
	metav1.TypeMeta `json:",inline"`
	// +optional
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   ClusterSpec   `json:"spec"`
	Status ClusterStatus `json:"status"`
}

type TlsClientConfig struct {
	Insecure bool   `json:"insecure"`
	CertData string `json:"cert_data"`
	KeyData  string `json:"key_data"`
	CAData   string `json:"ca_data"`
}

type ClusterSpec struct {
	WorkDir           string          `json:"work_dir"`
	Vip               string          `json:"vip"`
	Cri               Cri             `json:"container_runtime"`
	VipInterface      string          `json:"vip_interface"`
	KubePort          string          `json:"kube_port,omitempty"`
	ChartmuseumUrl    string          `json:"chartmuseum_url" yaml:"chartmuseum_url"`
	RegistryUrl       string          `json:"registry_url" yaml:"registry_url"`
	PipUrl            string          `json:"pip_url" yaml:"pip_url"`
	ExternalRepo      *bool           `json:"external_repo" yaml:"external_repo"`
	InstallPlugins    []string        `json:"install_plugins" yaml:"install_plugins"`
	CertSans          []string        `json:"cert_sans" yaml:"cert_sans"`
	BearerToken       string          `json:"bearer_token,omitempty"`
	KubeConfig        TlsClientConfig `json:"kubeconfig,omitempty"`
	GlobalSSHUsername string          `json:"global_ssh_username" yaml:"global_ssh_username"`
	GlobalSSHPassword string          `json:"global_ssh_password" yaml:"global_ssh_password"`
	NFS               NFS             `json:"nfs" yaml:"nfs"`
	Masters           []*VmNode       `json:"masters"`
	Workers           []*VmNode       `json:"workers"`
	Network           Network
}

type Cri struct {
	NeedInstall *bool   `json:"need_install" yaml:"need_install"`
	CriType     CriType `json:"cri_type" yaml:"cri_type"`
}

type CriType string

// nfs 配置
type NFS struct {
	Enable            *bool  `json:"enable" yaml:"enable"`
	ExternalNfsServer string `json:"external_nfs_server" yaml:"external_nfs_server"`
	ExternalNfsPath   string `json:"external_nfs_path" yaml:"external_nfs_path"`
	InternalNfsServer string `json:"internal_nfs_server" yaml:"internal_nfs_server"`
	InternalNfsPath   string `json:"internal_nfs_path" yaml:"internal_nfs_path"`
}

// VmNode include node info.
type VmNode struct {
	IP       string `json:"ip,omitempty" yaml:"ip"`
	Port     string `json:"port" yaml:"port"`
	Hostname string `json:"hostname,omitempty" yaml:"hostname"`
	User     string `json:"user,omitempty" yaml:"user"`
	Password string `json:"password,omitempty" yaml:"password"`
}
type ClusterStatus struct {
	Action ClusterAction `json:"action"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
type ClusterList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata"`

	Items []Cluster `json:"items"`
}

// Network is cluster network
type Network struct {
	Type             NetworkType    `json:"type,omitempty" yaml:"type"`
	CalicoIPIP       CalicoIPIPMode `json:"calico_ipip,omitempty" yaml:"calico_ipip"`
	PodCIDR          string         `json:"pod_cidr,omitempty" yaml:"pod_cidr"`
	ServiceCIDR      string         `json:"service_cidr,omitempty" yaml:"service_cidr"`
	ServiceDomain    string         `json:"service_domain,omitempty" yaml:"service_domain"`
	NetworkInterface string         `json:"network_interface" yaml:"network_interface"`
}

type ClusterInfo struct {
	CRI        string `json:"cri"`
	K8sVersion string `json:"k8s_version"`
}

// NetworkType identifies the cluster network type.
type NetworkType string

const (
	// NetworkTypeCalico uses calico as network plugin
	NetworkTypeCalico = "calico"
	// NetworkTypeCalicoTypha uses calico-typha as network plugin
	NetworkTypeCalicoTypha = "calico-typha"
)

// CalicoIPIPMode identifies calico ipip mode.
type CalicoIPIPMode string

const (
	// CalicoIPIPModeAlways enable ipip always
	CalicoIPIPModeAlways CalicoIPIPMode = "always"

	// CalicoIPIPModeOff disable ipip
	CalicoIPIPModeOff CalicoIPIPMode = "off"

	// CalicoIPIPModeCrossSubnet only when cross subnet
	CalicoIPIPModeCrossSubnet CalicoIPIPMode = "crosssubnet"
)

const ClusterCrd = `
apiVersion: apiextensions.k8s.io/v1
kind: CustomResourceDefinition
metadata:
  name: clusters.hhstu.github.com
spec:
  conversion:
    strategy: None
  group: hhstu.github.com
  names:
    kind: Cluster
    listKind: ClusterList
    plural: clusters
    shortNames:
    - c
    singular: cluster
  scope: Cluster
  versions:
  - additionalPrinterColumns:
    - jsonPath: .spec.Vip
      name: LB
      type: string
    - jsonPath: .metadata.creationTimestamp
      name: AGE
      type: date
    name: v1
    subresources:
      status: {}
    schema:
      openAPIV3Schema:
        properties:
          spec: 
            properties:
              load_balancer:
                 type: string
            type: object
            x-kubernetes-preserve-unknown-fields: true
        type: object  
        x-kubernetes-preserve-unknown-fields: true
    served: true
    storage: true

`

func (c *Cluster) GetMastersIps() []string {
	mastersIps := []string{}
	for _, master := range c.Spec.Masters {
		mastersIps = append(mastersIps, master.IP)
	}
	return mastersIps
}

func (c *Cluster) GetWorkersIps() []string {
	workersIps := []string{}
	for _, master := range c.Spec.Masters {
		workersIps = append(workersIps, master.IP)
	}
	return workersIps
}

func (c *Cluster) GetClusterIps() []string {
	clusterIps := []string{}
	for _, master := range append(c.Spec.Masters, c.Spec.Workers...) {
		clusterIps = append(clusterIps, master.IP)
	}
	return clusterIps
}

func (c *Cluster) GetAccessIp() string {
	// 纳管集群直接为 Vip
	if !c.IsAutoInstall() {
		return c.Spec.Vip
	}

	// 非纳管集群
	if c.Spec.Vip == "" && len(c.Spec.Masters) > 0 {
		return c.Spec.Masters[0].IP
	}
	// master节点 小于2时即使 Vip 不为空 也不部署 vip
	if 0 < len(c.Spec.Masters) && len(c.Spec.Masters) < 2 && c.Spec.Masters[0].IP != "" {
		return c.Spec.Masters[0].IP
	}

	return c.Spec.Vip
}

func (c *Cluster) Check() error {
	if c.Name == "" {
		return errors.New("name 不能为空")
	}

	if c.IsAutoInstall() {
		if err := c.CheckInstall(); err != nil {
			return err
		}
		// check InternalNfsServer
		if c.Spec.NFS.InternalNfsServer != "" {
			if ip := net.ParseIP(c.Spec.NFS.InternalNfsServer); ip == nil {
				return errors.New(fmt.Sprintf("internalNfsServer %s 格式错误，必须为 ipv4 格式", c.Spec.NFS.InternalNfsServer))
			}
			if _, err := c.GetVMNodeByIP(c.Spec.NFS.InternalNfsServer); err != nil {
				return errors.New("internalNfsServer 必须是 maters 或 workers 中存在的 IP")
			}
		}

	} else {
		if c.Spec.Vip == "" {
			return errors.New("纳管集群 loadBalancer 不能为空")
		}
	}

	return nil
}

func (c *Cluster) CheckInstall() error {
	if len(c.Spec.Masters) < 1 {
		return errors.New("master 节点个数不能小于 1")
	}
	ipMap := make(map[string]struct{})
	for _, ip := range c.GetClusterIps() {
		if ip == "" {
			return errors.New("节点 ip 不能为空")
		}
		address := net.ParseIP(ip)
		if address == nil {
			return errors.New(fmt.Sprintf("节点 ip %s 格式错误", ip))
		}
		if _, ok := ipMap[ip]; ok {
			return errors.New(fmt.Sprintf("节点 ip %s 重复", ip))
		}
		ipMap[ip] = struct{}{}
	}
	if c.Spec.Vip != "" {
		lb := net.ParseIP(c.Spec.Vip)
		if lb == nil {
			return errors.New(fmt.Sprintf("loadBalancer %s 格式错误", c.Spec.Vip))
		}
		if _, ok := ipMap[c.Spec.Vip]; ok {
			return errors.New(fmt.Sprintf("loadBalancer ip %s 与其他服务器重复", lb))
		}
		if len(c.Spec.Masters) < 2 {
			return errors.New("master 节点数小于 2 ， 无法部署高可用 IP，请置空 load_balancer 或添加 master ")
		}
	}

	hostnameMap := make(map[string]struct{})
	for _, vmNode := range append(c.Spec.Masters, c.Spec.Workers...) {
		if vmNode.Hostname != "" {
			if _, ok := hostnameMap[vmNode.Hostname]; ok {
				return errors.New(fmt.Sprintf("hostname %s 重复", vmNode.Hostname))
			}
			hostnameMap[vmNode.Hostname] = struct{}{}
		}
	}

	if c.Spec.RegistryUrl != "" {
		_, err := url.ParseRequestURI(c.Spec.RegistryUrl)
		if err != nil {
			return errors.Wrap(err, "registryUrl 格式错误")
		}
	}

	if c.Spec.ChartmuseumUrl != "" {
		_, err := url.ParseRequestURI(c.Spec.ChartmuseumUrl)
		if err != nil {
			return errors.Wrap(err, "chartmuseumUrl 格式错误")
		}
	}

	return nil
}

func (c *Cluster) IsManager() bool {
	if c.Annotations["manager"] == "yes" {
		return true
	}
	return false
}

func (c *Cluster) IsAutoInstall() bool {
	if b, ok := c.Annotations["kube-deploy/auto_install"]; ok && b == "true" {
		return true
	}
	return false
}
func (c *Cluster) GetVMNodeByIP(ip string) (*VmNode, error) {
	for _, node := range append(c.Spec.Masters, c.Spec.Workers...) {
		if node.IP == ip {
			if node.Password == "" {
				node.Password = c.Spec.GlobalSSHPassword
			}
			return node, nil
		}

	}
	return nil, errors.New("not found")
}
