// Package crdclientset
// 功能描述: 
// Date: 2022/5/20
// author: lixiaoming
package crdclientset

import (
	appv1ex "gitee.com/dce-paas/crd-client/generated/app/v1/informers/externalversions"
	esv1ex "gitee.com/dce-paas/crd-client/generated/elasticsearch/v1/informers/externalversions"
	hrv1ex "gitee.com/dce-paas/crd-client/generated/helm.fluxcd.io/v1/informers/externalversions"
	kbv1ex "gitee.com/dce-paas/crd-client/generated/kb/v1/informers/externalversions"
	kibanav1ex "gitee.com/dce-paas/crd-client/generated/kibana/v1/informers/externalversions"
	"time"
)

const DefaultResyncPeriod = 30

type CrdInformerset struct {
	appv1factory    appv1ex.SharedInformerFactory
	kbv1factory     kbv1ex.SharedInformerFactory
	hrv1factory     hrv1ex.SharedInformerFactory
	kibanav1factory kibanav1ex.SharedInformerFactory
	esfactory       esv1ex.SharedInformerFactory
}



func (i *CrdInformerset) AppV1Factory() appv1ex.SharedInformerFactory {
	return i.appv1factory
}

func (i *CrdInformerset) KbV1Factory() kbv1ex.SharedInformerFactory {
	return i.kbv1factory
}

func (i *CrdInformerset) HrV1Factory() hrv1ex.SharedInformerFactory {
	return i.hrv1factory
}

func (i *CrdInformerset) KibanaV1Factory() kibanav1ex.SharedInformerFactory {
	return i.kibanav1factory
}

func (i *CrdInformerset) EsV1Factory() esv1ex.SharedInformerFactory {
	return i.esfactory
}

func NewInformerForClientset(cs *CrdClientset) (*CrdInformerset, error) {
	var is CrdInformerset
	is.appv1factory = appv1ex.NewSharedInformerFactory(cs.appv1, DefaultResyncPeriod * time.Second)
	is.kbv1factory = kbv1ex.NewSharedInformerFactoryWithOptions(cs.kbv1, DefaultResyncPeriod * time.Second)
	is.hrv1factory = hrv1ex.NewSharedInformerFactory(cs.hrv1, DefaultResyncPeriod * time.Second)
	is.kibanav1factory = kibanav1ex.NewSharedInformerFactory(cs.kibanav1, DefaultResyncPeriod * time.Second)
	is.esfactory = esv1ex.NewSharedInformerFactory(cs.esv1, DefaultResyncPeriod * time.Second)
	return &is, nil
}

func NewInformerForClientsetWithResyncPeriod(cs *CrdClientset, period time.Duration) (*CrdInformerset, error) {
	var is CrdInformerset
	is.appv1factory = appv1ex.NewSharedInformerFactory(cs.appv1, period)
	is.kbv1factory = kbv1ex.NewSharedInformerFactoryWithOptions(cs.kbv1, period)
	is.hrv1factory = hrv1ex.NewSharedInformerFactory(cs.hrv1, period)
	is.esfactory = esv1ex.NewSharedInformerFactory(cs.esv1, period)
	return &is, nil
}