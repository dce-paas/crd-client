// Package clentset
// 功能描述:  crd clientset test
// Date: 2022/5/20
// author: lixiaoming
package crdclientset

import (
	"fmt"
	appv1 "gitee.com/dce-paas/crd-client/generated/app/v1/clientset/versioned"
	apptypev1 "gitee.com/dce-paas/crd-client/generated/app/v1/clientset/versioned/typed/app/v1"
	esv1 "gitee.com/dce-paas/crd-client/generated/elasticsearch/v1/clientset/versioned"
	v1 "gitee.com/dce-paas/crd-client/generated/elasticsearch/v1/clientset/versioned/typed/elasticsearch/v1"
	hrv1 "gitee.com/dce-paas/crd-client/generated/helm.fluxcd.io/v1/clientset/versioned"
	helmtypev1 "gitee.com/dce-paas/crd-client/generated/helm.fluxcd.io/v1/clientset/versioned/typed/helm.fluxcd.io/v1"
	kbv1 "gitee.com/dce-paas/crd-client/generated/kb/v1/clientset/versioned"
	kbtypev1 "gitee.com/dce-paas/crd-client/generated/kb/v1/clientset/versioned/typed/kb/v1"
	kibanav1 "gitee.com/dce-paas/crd-client/generated/kibana/v1/clientset/versioned"
	kibanatypev1 "gitee.com/dce-paas/crd-client/generated/kibana/v1/clientset/versioned/typed/kibana/v1"
	"k8s.io/client-go/discovery"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/util/flowcontrol"
)


type CrdClientset struct {
	*discovery.DiscoveryClient
	// informerset
	informerSet *CrdInformerset
	// client
	appv1    *appv1.Clientset
	kbv1     *kbv1.Clientset
	hrv1     *hrv1.Clientset
	kibanav1 *kibanav1.Clientset
	esv1     *esv1.Clientset
}

func (c *CrdClientset) InformerSet() *CrdInformerset {
	return c.informerSet
}

func (c *CrdClientset) AppV1()  apptypev1.AppV1Interface {
	return c.appv1.AppV1()
}

func (c *CrdClientset) KbV1() kbtypev1.HhstuV1Interface  {
	return c.kbv1.HhstuV1()
}

func (c *CrdClientset) HrV1() helmtypev1.HelmV1Interface {
	return c.hrv1.HelmV1()
}

func (c *CrdClientset) KibanaV1() kibanatypev1.KibanaV1Interface {
	return c.kibanav1.KibanaV1()
}

func (c *CrdClientset) EsV1() v1.ElasticsearchV1Interface {
	return c.esv1.ElasticsearchV1()
}


func NewForConfig(c *rest.Config) (*CrdClientset, error) {
	configShallowCopy := *c
	if configShallowCopy.RateLimiter == nil && configShallowCopy.QPS > 0 {
		if configShallowCopy.Burst <= 0 {
			return nil, fmt.Errorf("burst is required to be greater than 0 when RateLimiter is not set and QPS is set to greater than 0")
		}
		configShallowCopy.RateLimiter = flowcontrol.NewTokenBucketRateLimiter(configShallowCopy.QPS, configShallowCopy.Burst)
	}
	var cs CrdClientset
	var err error

	cs.appv1, err = appv1.NewForConfig(&configShallowCopy)
	if err != nil {
		return nil, err
	}

	cs.kbv1, err = kbv1.NewForConfig(&configShallowCopy)
	if err != nil {
		return nil, err
	}

	cs.hrv1, err = hrv1.NewForConfig(&configShallowCopy)
	if err != nil {
		return nil, err
	}

	cs.kibanav1, err = kibanav1.NewForConfig(&configShallowCopy)
	if err != nil {
		return nil, err
	}

	cs.esv1, err = esv1.NewForConfig(&configShallowCopy)
	if err != nil {
		return nil, err
	}

	cs.DiscoveryClient, err = discovery.NewDiscoveryClientForConfig(&configShallowCopy)
	if err != nil {
		return nil, err
	}

	cs.informerSet, err = NewInformerForClientset(&cs)
	if err != nil {
		return nil, err
	}
	return &cs, nil
}