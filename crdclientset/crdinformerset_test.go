// Package crdclientset
// 功能描述: informerset 测试
// Date: 2022/5/20
// author: lixiaoming
package crdclientset

import (
	"context"
	"fmt"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/util/runtime"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/cache"
	"testing"
)

func getConfig() *rest.Config{
	apiProtocol:= "https"
	apiHost := "47.92.202.208:14008"
	apiToken := "eyJhbGciOiJSUzI1NiIsImtpZCI6IkpuMDdKbGhKaHdPcEdzWmlwUzFLZ0lwVC1pZlBPRDFYT2d0bHl5UUpWUlEifQ.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJrdWJlLXN5c3RlbSIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VjcmV0Lm5hbWUiOiJhZG1pbi10b2tlbi1zdGc0YiIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VydmljZS1hY2NvdW50Lm5hbWUiOiJhZG1pbiIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VydmljZS1hY2NvdW50LnVpZCI6ImRjZDA0NGRkLTEyNzktNDczYy1hNGY1LTM0N2E0NzEwOTYzZSIsInN1YiI6InN5c3RlbTpzZXJ2aWNlYWNjb3VudDprdWJlLXN5c3RlbTphZG1pbiJ9.pJ1m6k78WGPWsVnUNreUS4m4rT_tn0qLxcNFaah4Lk6SGHJgadX579tkkgF9IIbfUgUyq9w1GHie9w5ynYKojDPShgx4taWwS5CZqA81UwXHV0GrAkKNOB6X6Uxargs9na2H-AMcBs_s7e34A2R4WJXC5LqEK6AO8TXq34jwV8zBLy8oliSgXQw8Xa1x8o3K3NkuAerKC02420t9ISsbcA-RbPyG6Q1cM4Cwh0U1NZNlDoPzXkEenWzNytdN-EzO_mfRJI3-aH4MTDs53axlPNvosQrDJ4ZeBK2-xLJycaioehAbCeK2hR3iRT-is9MZ1qpevix7jAooD-ND5ZQAgQ"

	return  &rest.Config{
		Host:            fmt.Sprintf("%s://%s", apiProtocol, apiHost),
		BearerToken:     apiToken,
		TLSClientConfig: rest.TLSClientConfig{
			Insecure: true,
		},
	}
}

func TestHelmreleaseClient(t *testing.T) {
	crdClientset, err := NewForConfig(getConfig())
	if err != nil {
		fmt.Println("err")
		return
	}
	list, err := crdClientset.HrV1().HelmReleases("").List(context.Background(), v1.ListOptions{})
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	fmt.Println(len(list.Items))

	informerSet := crdClientset.InformerSet()
	factory := informerSet.HrV1Factory()
	hrInformer := factory.Helm().V1().HelmReleases()
	stopper := make(chan struct{})

	informer := hrInformer.Informer()
	go factory.Start(stopper)

	if !cache.WaitForCacheSync(stopper, informer.HasSynced) {
		runtime.HandleError(fmt.Errorf("Timed out waiting for caches to sync"))
		return
	}

	lister := hrInformer.Lister()
	hrList, err := lister.List(labels.Everything())
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	fmt.Println(len(hrList))
}

func TestKibanaClient(t *testing.T)  {
	crdClientset, err := NewForConfig(getConfig())
	if err != nil {
		fmt.Println("err")
		return
	}
	list, err := crdClientset.KibanaV1().Kibanas("").List(context.Background(), v1.ListOptions{})
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	fmt.Println(len(list.Items))

	informerSet := crdClientset.InformerSet()
	factory := informerSet.KibanaV1Factory()
	hrInformer := factory.Kibana().V1().Kibanas()
	stopper := make(chan struct{})

	informer := hrInformer.Informer()
	go factory.Start(stopper)

	if !cache.WaitForCacheSync(stopper, informer.HasSynced) {
		runtime.HandleError(fmt.Errorf("Timed out waiting for caches to sync"))
		return
	}

	lister := hrInformer.Lister()
	hrList, err := lister.List(labels.Everything())
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	fmt.Println(len(hrList))
}

func TestEsClient(t *testing.T)  {
	crdClientset, err := NewForConfig(getConfig())
	if err != nil {
		fmt.Println("err")
		return
	}
	list, err := crdClientset.EsV1().Elasticsearches("").List(context.Background(), v1.ListOptions{})
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	fmt.Println(len(list.Items))

	informerSet := crdClientset.InformerSet()
	factory := informerSet.EsV1Factory()
	hrInformer := factory.Elasticsearch().V1().Elasticsearches()
	stopper := make(chan struct{})

	informer := hrInformer.Informer()
	go factory.Start(stopper)

	if !cache.WaitForCacheSync(stopper, informer.HasSynced) {
		runtime.HandleError(fmt.Errorf("Timed out waiting for caches to sync"))
		return
	}

	lister := hrInformer.Lister()
	hrList, err := lister.List(labels.Everything())
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	fmt.Println(len(hrList))
}