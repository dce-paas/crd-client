# 用于对CRD对象生成clientSet及Informer

## 1. 生成CRD的client, informer, lister
1. 添加crd项目的对象对应包至 /apis,  格式为 
   ```/apis/gourpName/version/```
2. 添加文件中的代码生成注解
   
   -  crd_types:
   ```
   // +genclient
   // +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
   type Crd struct
   
   // +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
   type CrdList struct
   ```
   - doc
   ```
   // +k8s:deepcopy-gen=package
   // +groupName=app.customer.domain  
   package v1
   ```
   - groupversion_info
   ```
   //+kubebuilder:object:generate=true
   //+groupName=app.customer.domain
   package v1
   ```
3. /hack/tools.go 引用 k8s.io/code-generator,生成vendor
4. 执行命令进行代码生成
   ```
   update-codegen
   ```
## 2. 添加client,informer到crdClientset
参照crdclientset.go crdinformerset.go文件中的格式,添加对应的的代码

